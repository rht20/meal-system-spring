<%--
  Created by IntelliJ IDEA.
  User: rakibul.hasan
  Date: 3/1/20
  Time: 2:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>
        <spring:message code="label.title.menuForm"/>
    </title>

    <link href="${pageContext.request.contextPath}/resources/statics/css/style.css" rel="stylesheet">
</head>

<body>
<div class="center">
    <h2 style="width: 100%; text-align: center">${menu.day}, ${menu.mealType.name}</h2>
    <br/>
    <br/>

    <form:form action="/menu/update" method="POST" cssClass="btn-group" modelAttribute="menu">

        <form:input path="id" hidden="hidden"/>

        <form:checkboxes path="itemList" items="${items}" itemLabel="name" itemValue="id" element="li"/>

        <br/>

        <table style="width: 100%;" id="table-collapse">
            <tr>
                <td style="width: 50%;">
                    <form:button formaction="/menu/list">
                        <spring:message code="label.cancel"/>
                    </form:button>
                </td>

                <td style="width: 50%;">
                    <form:button>
                        <spring:message code="label.submit"/>
                    </form:button>
                </td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>
