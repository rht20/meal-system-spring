<!--

  User: rakibul.hasan
  Date: 3/11/20
  Time: 9:38 AM
-->
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>
        <spring:message code="label.title.home"/>
    </title>

    <link href="${pageContext.request.contextPath}/resources/statics/css/style.css" rel="stylesheet">
</head>

<body>
<div class="center">
    <h1 style="width: 100%; text-align: center; padding-left: 7%;">
        <spring:message code="label.caption.home"/>
    </h1>

    <a href="/items/list" class="button">
        <spring:message code="label.items"/>
    </a>
    <br/>

    <a href="/mealTypes/list" class="button">
        <spring:message code="label.mealTypes"/>
    </a>
    <br/>

    <a href="/menu/list" class="button">
        <spring:message code="label.menu"/>
    </a>
    <br/>

    <a href="/logout" class="button">
        <spring:message code="label.logout"/>
    </a>
</div>
</body>
</html>
