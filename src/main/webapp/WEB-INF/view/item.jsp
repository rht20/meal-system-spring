<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>
        <spring:message code="label.title.item"/>
    </title>

    <link href="${pageContext.request.contextPath}/resources/statics/css/style.css" rel="stylesheet">
</head>

<body>
<div class="center">
    <h1 style="width: 100%; text-align: center">
        <spring:message code="label.caption.item"/>
    </h1>

    <table style="width: 100%;" id="table-collapse">
        <c:forEach items="${items}" var="item">
            <tr style="width: 100%;">
                <td style="width: 34%;">
                    <h2>${item.name}</h2>
                </td>

                <td style="width: 33%;">
                    <c:url var="editItemUrl" value="/items/edit">
                        <c:param name="id" value="${item.id}"/>
                    </c:url>

                    <a href="${editItemUrl}" class="button">
                        <spring:message code="label.update"/>
                    </a>
                </td>

                <td style="width: 33%;">
                    <c:url var="removeItemUrl" value="/items/remove">
                        <c:param name="id" value="${item.id}"/>
                    </c:url>

                    <a href="${removeItemUrl}" class="button remove-button">
                        <spring:message code="label.remove"/>
                    </a>
                </td>
            </tr>
        </c:forEach>
    </table>

    <br/>
    <br/>
    <br/>

    <table style="width: 100%;" class="btn-group" id="table-collapse">
        <tr style="border: 1px;">
            <td style="width: 50%; text-align: center;">
                <a href="/home" class="button">
                    <spring:message code="label.home"/>
                </a>
            </td>

            <td style="width: 50%; text-align: center">
                <a href="/items/edit" class="button">
                    <spring:message code="label.addNewItem"/>
                </a>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
