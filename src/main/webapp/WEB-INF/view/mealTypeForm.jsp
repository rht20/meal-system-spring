<%--
  Created by IntelliJ IDEA.
  User: rakibul.hasan
  Date: 3/1/20
  Time: 4:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>
        <spring:message code="label.title.mealTypeForm"/>
    </title>

    <link href="${pageContext.request.contextPath}/resources/statics/css/style.css" rel="stylesheet">
</head>

<body>
<div class="center">
    <form:form action="/mealTypes/edit" method="post" modelAttribute="mealType">
        <form:input path="id" hidden="hidden"/>

        <form:errors path="name" cssStyle="color: red"/>
        <form:input path="name" type="text" style="width: 100%; font-size: 20px;"/>

        <br/>
        <br/>

        <table style="width: 100%;" class="btn-group" id="table-collapse">
            <tr style="border: 1px;">
                <td style="width: 50%; text-align: center;">
                    <form:button formaction="/mealTypes/list">
                        <spring:message code="label.cancel"/>
                    </form:button>
                </td>

                <td style="width: 50%; text-align: center">
                    <form:button>
                        <spring:message code="label.submit"/>
                    </form:button>
                </td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>
