<%--
  Created by IntelliJ IDEA.
  User: rht_20
  Date: 2/26/20
  Time: 9:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>
        <spring:message code="label.title.menu"/>
    </title>

    <link href="${pageContext.request.contextPath}/resources/statics/css/style.css" rel="stylesheet">
</head>

<body>
    <div class="center">
        <h1 style="width: 100%; text-align: center">
            <spring:message code="label.tableCaption.menu"/>
        </h1>

        <table class="btn-group" id="table-collapse">
            <tr>
                <td style="width: 20%; text-align: center">
                    <h3>
                        <spring:message code="label.day"/>
                    </h3>
                </td>

                <td style="width: 20%; text-align: center">
                    <h3>
                        <spring:message code="label.mealType"/>
                    </h3>
                </td>

                <td style="width: 40%; text-align: center">
                    <h3>
                        <spring:message code="label.items"/>
                    </h3>
                </td>

                <td style="width: 20%; text-align: center">
                    <h3>
                        <spring:message code="label.remove"/>
                    </h3>
                </td>
            </tr>

            <c:forEach items="${menus}" var="menu">
                <tr>
                    <td style="width: 20%; text-align: center">${menu['day']}</td>
                    <td style="width: 20%; text-align: center">${menu['mealType']}</td>
                    <td style="width: 40%; text-align: center">${menu['items']}</td>

                    <td style="width: 20%; text-align: center">
                        <c:url var="removeMenuUrl" value="/menu/remove">
                            <c:param name="id" value="${menu['menuId']}"/>
                        </c:url>

                        <a href="${removeMenuUrl}" class="button remove-button">
                            <spring:message code="label.remove"/>
                        </a>
                    </td>
                </tr>
            </c:forEach>
        </table>

        <br/>
        <br/>
        <br/>
        <br/>

        <h2 style="width: 100%; text-align: center">
            <spring:message code="label.selectCaption.menu"/>
        </h2>

        <table class="btn-group" id="table-collapse">
            <tr>
                <form:form action="/menu/edit" method="post" modelAttribute="menu">
                    <td style="width: 30%;">
                        <form:errors path="day" cssStyle="color: red"/>
                        <form:select path="day">
                            <form:option value="">
                                <spring:message code="label.selectDay"/>
                            </form:option>

                            <form:options items="${days}"/>
                        </form:select>
                    </td>

                    <td style="width: 40%;">
                        <form:errors path="mealType" cssStyle="color: red"/>
                        <form:select path="mealType">
                            <form:option value="">
                                <spring:message code="label.selectMealType"/>
                            </form:option>

                            <form:options items="${mealTypes}" itemLabel="name" itemValue="id"/>
                        </form:select>
                    </td>

                    <td style="width: 20%;">
                        <form:button>
                            <spring:message code="label.submit"/>
                        </form:button>
                    </td>
                </form:form>
            </tr>
        </table>

        <br/>
        <br/>
        <br/>
        <br/>

        <a href="/home" class="button">
            <spring:message code="label.home"/>
        </a>
    </div>
</body>
</html>
