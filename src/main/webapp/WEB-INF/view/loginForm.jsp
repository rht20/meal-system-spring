<%--
  Created by IntelliJ IDEA.
  User: rakibul.hasan
  Date: 2/26/20
  Time: 2:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
  <title>
    <spring:message code="label.title.login"/>
  </title>

  <link href="${pageContext.request.contextPath}/resources/statics/css/style.css" rel="stylesheet">
</head>
<body>
<div class="center">
  <form:form action="/login" method="post" modelAttribute="user">
    <form:errors path="uname" cssStyle="color: red"/>

    <table style="width: 100%;" id="table-collapse">
      <tr style="width: 100%;">
        <td style="width: 40%;">
          <spring:message code="label.userName"/>
        </td>

        <td style="width: 60%;">
            <form:input path="uname" type="text" cssStyle="width: 100%;"/>
        </td>
      </tr>

      <tr>
        <td style="width: 40%;">
          <spring:message code="label.password"/>
        </td>

        <td style="width: 60%;">
          <form:input path="password" type="password" cssStyle="width: 100%;"/>
        </td>
      </tr>
    </table>

    <br/>
    <br/>

    <form:button class="button">
      <spring:message code="label.login"/>
    </form:button>
  </form:form>
</div>
</body>
</html>
