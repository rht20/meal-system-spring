package net.therap.mealSystem.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
@Entity
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
    private MealType mealType;

    private Day day;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Item> itemList;

    public Menu() {
        this.itemList = new ArrayList<>();
    }

    public Menu(MealType mealType, Day day, List<Item> itemList) {
        this.mealType = mealType;
        this.day = day;
        this.itemList = itemList;
    }

    public Menu(MealType mealType, Day day) {
        this.mealType = mealType;
        this.day = day;
        this.itemList = new ArrayList<>();
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMealType(MealType mealType) {
        this.mealType = mealType;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public int getId() {
        return id;
    }

    public MealType getMealType() {
        return mealType;
    }

    public Day getDay() {
        return day;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public boolean isNew() {
        return id == 0;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", mealType=" + mealType +
                ", day=" + day +
                ", itemList=" + itemList +
                '}';
    }
}
