package net.therap.mealSystem.model;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
public enum Day {
    SATURDAY, SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY
}
