package net.therap.mealSystem.model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author rakibul.hasan
 * @since 3/2/20
 */
@Entity
public class User {

    @Id
    private String uname;

    private String password;

    public void setUname(String uname) {
        this.uname = uname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUname() {
        return uname;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "User{" +
                "uname='" + uname + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
