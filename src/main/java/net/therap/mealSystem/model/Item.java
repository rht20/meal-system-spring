package net.therap.mealSystem.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
@Entity
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    public Item() {
    }

    public Item(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isNew() {
        return id == 0;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public int hash() {
        System.out.println("hash() " + this.id);
        return this.id;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;

        } else if (!Item.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        Item item = (Item) obj;
        System.out.println("equals() " + item);
        if (this.getId() != item.getId() || this.getName() != item.getName()) {
            return false;
        }

        return true;
    }
}
