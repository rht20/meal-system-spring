package net.therap.mealSystem.editor;

import net.therap.mealSystem.model.Item;
import net.therap.mealSystem.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

/**
 * @author rakibul.hasan
 * @since 3/12/20
 */
@Component
public class ItemEditor extends PropertyEditorSupport {

    @Autowired
    ItemService itemService;

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Item item = itemService.getItemById(Integer.parseInt(text));
        setValue(item);
    }
}
