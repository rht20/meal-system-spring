package net.therap.mealSystem.editor;

import net.therap.mealSystem.model.MealType;
import net.therap.mealSystem.service.MealTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

/**
 * @author rakibul.hasan
 * @since 3/12/20
 */
@Component
public class MealTypeEditor extends PropertyEditorSupport {

    @Autowired
    private MealTypeService mealTypeService;

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (text == null || text.equals("")) {
            return;
        }

        MealType mealType = mealTypeService.getMealTypeById(Integer.parseInt(text));
        setValue(mealType);
    }
}
