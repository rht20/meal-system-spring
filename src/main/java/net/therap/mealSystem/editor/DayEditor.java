package net.therap.mealSystem.editor;

import net.therap.mealSystem.model.Day;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

/**
 * @author rakibul.hasan
 * @since 3/12/20
 */
@Component
public class DayEditor extends PropertyEditorSupport {

    public void setAsText(String text) throws IllegalArgumentException {
        if (text == null || text.equals("")) {
            return;
        }

        for (Day day : Day.values()) {
            if (day.name().equals(text)) {
                setValue(day);
            }
        }
    }
}
