package net.therap.mealSystem.dao;

import net.therap.mealSystem.model.Day;
import net.therap.mealSystem.model.Item;
import net.therap.mealSystem.model.MealType;
import net.therap.mealSystem.model.Menu;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/16/20
 */
@Repository
public class MenuDao {

    @PersistenceContext
    private EntityManager em;

    public List<Menu> getMenus() {
        TypedQuery<Menu> query = em.createQuery("FROM Menu", Menu.class);
        return query.getResultList();
    }

    public Menu getMenuById(int id) {
        return em.find(Menu.class, id);
    }

    public Menu getMenuByMealTypeAndDay(MealType mealType, Day day) {
        TypedQuery<Menu> query = em.createQuery("SELECT M FROM Menu AS M WHERE M.mealType = :mealType AND M.day = :day", Menu.class)
                .setParameter("mealType", mealType)
                .setParameter("day", day);

        List<Menu> menuList = query.getResultList();

        return menuList.isEmpty() ? new Menu(mealType, day) : menuList.get(0);
    }

    @Transactional
    public Menu addMenu(Menu menu) {
        em.persist(menu);
        em.flush();

        return menu;
    }

    @Transactional
    public void removeMenu(Menu menu) {
        menu = em.find(Menu.class, menu.getId());
        em.remove(menu);
    }

    @Transactional
    public void removeByItem(Item item) {
        TypedQuery<Menu> query = em.createQuery("SELECT M FROM Menu AS M", Menu.class);
        List<Menu> menuList = query.getResultList();

        boolean flag = false;
        for (Menu menu : menuList) {
            for (Item item1 : menu.getItemList()) {
                if (item.getId() == item1.getId()) {
                    menu.getItemList().remove(item1);
                    flag = true;
                    break;
                }
            }
            if (flag) {
                break;
            }
        }
    }

    @Transactional
    public void removeByMealType(MealType mealType) {
        TypedQuery<Menu> query = em.createQuery("SELECT M FROM Menu AS M WHERE M.mealType = :mealType", Menu.class)
                .setParameter("mealType", mealType);
        List<Menu> menuList = query.getResultList();

        for (Menu menu : menuList) {
            em.remove(menu);
        }
    }

    @Transactional
    public Menu updateMenu(Menu menu) {
        em.merge(menu);
        em.flush();

        return menu;
    }
}
