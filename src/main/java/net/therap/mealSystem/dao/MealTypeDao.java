package net.therap.mealSystem.dao;

import net.therap.mealSystem.model.MealType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/16/20
 */
@Repository
public class MealTypeDao {

    @PersistenceContext
    private EntityManager em;

    public List<MealType> getMealTypes() {
        TypedQuery<MealType> query = em.createQuery("FROM MealType", MealType.class);
        return query.getResultList();
    }

    public MealType getMealTypeById(int id) {
        return em.find(MealType.class, id);
    }

    @Transactional
    public MealType addMealType(MealType mealType) {
        em.persist(mealType);
        em.flush();

        return mealType;
    }

    @Transactional
    public MealType removeMealType(MealType mealType) {
        em.remove(em.contains(mealType)? mealType : em.merge(mealType));

        return mealType;
    }

    @Transactional
    public MealType updateMealType(MealType mealType) {
        em.merge(mealType);
        em.flush();

        return mealType;
    }
}
