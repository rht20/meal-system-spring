package net.therap.mealSystem.dao;

import net.therap.mealSystem.model.Item;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/16/20
 */
@Repository
public class ItemDao {

    @PersistenceContext
    private EntityManager em;

    public List<Item> getItems() {
        TypedQuery<Item> query = em.createQuery("FROM Item", Item.class);
        return query.getResultList();
    }

    public Item getItemById(int id) {
        return em.find(Item.class, id);
    }

    @Transactional
    public Item addItem(Item item) {
        em.persist(item);
        em.flush();

        return item;
    }

    @Transactional
    public Item removeItem(Item item) {
        em.remove(em.contains(item)? item : em.merge(item));

        return item;
    }

    @Transactional
    public Item updateItem(Item item) {
        em.merge(item);
        em.flush();

        return item;
    }
}
