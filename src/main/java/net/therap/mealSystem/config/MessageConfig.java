package net.therap.mealSystem.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

/**
 * @author rakibul.hasan
 * @since 3/11/20
 */
@Configuration
public class MessageConfig {

    @Bean(name = "messageSource")
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasenames("messages/messages", "messages/errors");
        messageSource.setDefaultEncoding("UTF-8");

        return messageSource;
    }
}
