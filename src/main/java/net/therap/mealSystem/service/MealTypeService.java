package net.therap.mealSystem.service;

import net.therap.mealSystem.dao.MealTypeDao;
import net.therap.mealSystem.dao.MenuDao;
import net.therap.mealSystem.model.MealType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Objects;

/**
 * @author rakibul.hasan
 * @since 2/11/20
 */
@Service
public class MealTypeService {

    @Autowired
    private MealTypeDao mealTypeDao;

    @Autowired
    private MenuDao menuDao;

    public List<MealType> getMealTypes() {
        return mealTypeDao.getMealTypes();
    }

    public MealType getMealTypeById(int id) {
        if (id == -1) {
            return new MealType();
        }

        MealType mealType = mealTypeDao.getMealTypeById(id);
        if (Objects.isNull(mealType)) {
            throw new NoResultException();
        }

        return mealType;
    }

    public MealType saveOrUpdate(MealType mealType) {
        MealType persistedMealType;

        if (mealType.isNew()) {
            persistedMealType = mealTypeDao.addMealType(mealType);

        } else {
            persistedMealType = mealTypeDao.updateMealType(mealType);
        }

        return persistedMealType;
    }

    public void removeMealType(MealType mealType) {
        menuDao.removeByMealType(mealType);
        mealTypeDao.removeMealType(mealType);
    }
}
