package net.therap.mealSystem.service;

import net.therap.mealSystem.dao.ItemDao;
import net.therap.mealSystem.dao.MenuDao;
import net.therap.mealSystem.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Objects;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
@Service
public class ItemService {

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private MenuDao menuDao;

    public List<Item> getItems() {
        return itemDao.getItems();
    }

    public Item getItemById(int id) {
        if (id == -1) {
            return new Item();
        }

        Item item = itemDao.getItemById(id);

        if (Objects.isNull(item)) {
            throw new NoResultException();
        }

        return item;
    }

    public Item saveOrUpdate(Item item) {
        Item persistedItem;

        if (item.isNew()) {
            persistedItem = itemDao.addItem(item);

        } else {
            persistedItem = itemDao.updateItem(item);
        }

        return persistedItem;
    }

    public void removeItem(Item item) {
        menuDao.removeByItem(item);
        itemDao.removeItem(item);
    }
}
