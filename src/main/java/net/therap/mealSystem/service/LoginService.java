package net.therap.mealSystem.service;

import net.therap.mealSystem.model.User;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * @author rakibul.hasan
 * @since 3/2/20
 */
@Service
public class LoginService {

    @PersistenceContext
    private EntityManager em;

    public boolean check(User user) {
        TypedQuery<User> query = em.createQuery("SELECT U FROM User AS U WHERE U.uname = :uname AND U.password = :password", User.class)
                .setParameter("uname", user.getUname())
                .setParameter("password", user.getPassword());

        return (!query.getResultList().isEmpty());
    }
}
