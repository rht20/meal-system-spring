package net.therap.mealSystem.service;

import net.therap.mealSystem.dao.MenuDao;
import net.therap.mealSystem.model.Day;
import net.therap.mealSystem.model.Item;
import net.therap.mealSystem.model.MealType;
import net.therap.mealSystem.model.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
@Service
public class MenuService {

    @Autowired
    private MenuDao menuDao;

    public List<Menu> getMenus() {
        return menuDao.getMenus();
    }

    public Menu getMenuById(int id) {
        return menuDao.getMenuById(id);
    }

    public Menu getMenuByMealTypeAndDay(MealType mealType, Day day) {
        return menuDao.getMenuByMealTypeAndDay(mealType, day);
    }

    public Menu addMenu(Menu menu) {
        return menuDao.addMenu(menu);
    }

    public void removeMenu(int menuId) {
        Menu menu = new Menu();
        menu.setId(menuId);

        menuDao.removeMenu(menu);
    }

    public Menu updateMenu(Menu menu) {
        return menuDao.updateMenu(menu);
    }
}
