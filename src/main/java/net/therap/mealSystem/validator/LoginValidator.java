package net.therap.mealSystem.validator;

import net.therap.mealSystem.model.User;
import net.therap.mealSystem.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class LoginValidator implements Validator {

    @Autowired
    LoginService loginService;

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User) target;

        if (user.getUname().equals("") || user.getPassword().equals("") || !loginService.check(user)) {
            errors.rejectValue("uname", "label.error.login");
        }
    }
}
