package net.therap.mealSystem.validator;

import net.therap.mealSystem.model.Menu;
import org.springframework.stereotype.Component;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author rakibul.hasan
 * @since 3/12/20
 */
@Component
public class MenuValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Menu.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Menu menu = (Menu) target;

        if (menu.getDay() == null) {
            errors.rejectValue("day", "label.day.menu");
        }
        if (menu.getMealType() == null) {
            errors.rejectValue("mealType", "label.mealType.menu");
        }
    }
}
