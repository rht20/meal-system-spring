package net.therap.mealSystem.validator;

import net.therap.mealSystem.model.Item;
import net.therap.mealSystem.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.List;

/**
* @author rakibul.hasan
* @since 3/11/20
*/
@Component
public class ItemValidator implements Validator {

    @Autowired
    ItemService itemService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Item.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Item item = (Item) target;

        if (item.getName() == null || item.getName().equals("")) {
            errors.rejectValue("name", "label.name.empty.item");
        }

        List<Item> items = itemService.getItems();

        for (Item item1 : items) {
            if (item1.getName().equals(item.getName())) {
                errors.rejectValue("name", "label.name.exist.item");
                break;
            }
        }
    }
}
