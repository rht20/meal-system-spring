package net.therap.mealSystem.validator;

import net.therap.mealSystem.model.MealType;
import net.therap.mealSystem.service.MealTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.List;

/**
 * @author rakibul.hasan
 * @since 3/12/20
 */
@Component
public class MealTypeValidator implements Validator {

    @Autowired
    private MealTypeService mealTypeService;

    @Override
    public boolean supports(Class<?> clazz) {
        return MealType.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MealType mealType = (MealType) target;

        if (mealType.getName() == null || mealType.getName().equals("")) {
            errors.rejectValue("name", "label.name.empty.mealType");
        }

        List<MealType> mealTypes = mealTypeService.getMealTypes();
        for (MealType mealType1 : mealTypes) {
            if (mealType1.getName().equals(mealType.getName())) {
                errors.rejectValue("name", "label.name.exist.mealType");
                break;
            }
        }
    }
}
