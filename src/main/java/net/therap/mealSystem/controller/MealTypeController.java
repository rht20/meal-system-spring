package net.therap.mealSystem.controller;

import net.therap.mealSystem.model.MealType;
import net.therap.mealSystem.service.MealTypeService;
import net.therap.mealSystem.utility.Utility;
import net.therap.mealSystem.validator.MealTypeValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author rakibul.hasan
 * @since 3/10/20
 */
@Controller
@RequestMapping(value = "/mealTypes")
public class MealTypeController {

    private static final String SHOW = "mealType";
    private static final String EDIT = "mealTypeForm";

    private static final String LIST_URL = "/mealTypes/list";

    private static final String COMMAND = "mealType";

    @Autowired
    private MealTypeService mealTypeService;

    @Autowired
    private MealTypeValidator mealTypeValidator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(mealTypeValidator);
    }

    @RequestMapping(value = "/list", method = {RequestMethod.GET, RequestMethod.POST})
    public String list(ModelMap modelMap) {
        List<MealType> mealTypes = mealTypeService.getMealTypes();
        modelMap.addAttribute("mealTypes", mealTypes);

        return SHOW;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String edit(@RequestParam(value = "id", defaultValue = "-1") int mealTypeId,
                       ModelMap modelMap) {

        MealType mealType = mealTypeService.getMealTypeById(mealTypeId);

        setupReferenceData(mealType, modelMap);

        return EDIT;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String edit(@Validated @ModelAttribute(COMMAND) MealType mealType,
                       Errors errors,
                       ModelMap modelMap) {

        if (errors.hasErrors()) {
            setupReferenceData(mealType, modelMap);
            return EDIT;
        }

        mealTypeService.saveOrUpdate(mealType);

        return Utility.redirect(LIST_URL);
    }

    @RequestMapping(value = "/remove", method = RequestMethod.GET)
    public String remove(@RequestParam(value = "id") int mealTypeId) {
        MealType mealType = mealTypeService.getMealTypeById(mealTypeId);
        mealTypeService.removeMealType(mealType);

        return Utility.redirect(LIST_URL);
    }

    private void setupReferenceData(MealType mealType, ModelMap modelMap) {
        modelMap.put(COMMAND, mealType);
    }
}
