package net.therap.mealSystem.controller;

import net.therap.mealSystem.model.Item;
import net.therap.mealSystem.service.ItemService;
import net.therap.mealSystem.utility.Utility;
import net.therap.mealSystem.validator.ItemValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author rakibul.hasan
 * @since 3/10/20
 */
@Controller
@RequestMapping(value = "/items")
public class ItemController {

    private static final String SHOW = "item";
    private static final String EDIT = "itemForm";

    private static final String LIST_URL = "/items/list";

    private static final String COMMAND = "item";

    @Autowired
    private ItemService itemService;

    @Autowired
    private ItemValidator itemValidator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(itemValidator);
    }

    @RequestMapping(value = "/list", method = {RequestMethod.GET, RequestMethod.POST})
    public String list(ModelMap modelMap) {
        List<Item> items = itemService.getItems();
        modelMap.put("items", items);

        return SHOW;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String edit(@RequestParam(value = "id", defaultValue = "-1") int itemId,
                       ModelMap modelMap) {

        Item item = itemService.getItemById(itemId);

        setupReferenceData(item, modelMap);

        return EDIT;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String edit(@Validated @ModelAttribute(COMMAND) Item item,
                          Errors errors,
                          ModelMap modelMap) {

        if (errors.hasErrors()) {
            setupReferenceData(item, modelMap);
            return EDIT;
        }

        itemService.saveOrUpdate(item);

        return Utility.redirect(LIST_URL);
    }

    @RequestMapping(value = "/remove", method = RequestMethod.GET)
    public String remove(@RequestParam(value = "id") int itemId) {
        Item item = itemService.getItemById(itemId);
        itemService.removeItem(item);

        return Utility.redirect(LIST_URL);
    }

    private void setupReferenceData(Item item, ModelMap modelMap) {
        modelMap.put(COMMAND, item);
    }
}
