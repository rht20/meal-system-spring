package net.therap.mealSystem.controller;

import net.therap.mealSystem.model.User;
import net.therap.mealSystem.utility.Utility;
import net.therap.mealSystem.validator.LoginValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * @author rakibul.hasan
 * @since 3/10/20
 */

@Controller
public class AuthController {

    private static final String LOGIN = "loginForm";
    private static final String HOME = "home";

    @Autowired
    LoginValidator loginValidator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(loginValidator);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String root(ModelMap modelMap, HttpSession session) {
        if (session.getAttribute("uname") != null) {
            return HOME;
        }

        modelMap.put("user", new User());
        return LOGIN;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@Validated @ModelAttribute(value = "user") User user,
                        Errors errors,
                        ModelMap modelMap,
                        HttpSession session) {

        if (errors.hasErrors()) {
            modelMap.put("user", user);
            return LOGIN;
        }

        session.setAttribute("uname", user.getUname());

        return HOME;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpSession session) {
        session.invalidate();

        return Utility.redirect("/");
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home() {
        return HOME;
    }
}
