package net.therap.mealSystem.controller;

import net.therap.mealSystem.editor.DayEditor;
import net.therap.mealSystem.editor.ItemEditor;
import net.therap.mealSystem.editor.MealTypeEditor;
import net.therap.mealSystem.model.Day;
import net.therap.mealSystem.model.Item;
import net.therap.mealSystem.model.MealType;
import net.therap.mealSystem.model.Menu;
import net.therap.mealSystem.service.ItemService;
import net.therap.mealSystem.service.MealTypeService;
import net.therap.mealSystem.service.MenuService;
import net.therap.mealSystem.utility.Utility;
import net.therap.mealSystem.validator.MenuValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @author rakibul.hasan
 * @since 3/10/20
 */
@Controller
@RequestMapping(value = "/menu")
public class MenuController {

    private static final String SHOW = "menu";
    private static final String EDIT = "menuForm";

    private static final String LIST_URL = "/menu/list";

    @Autowired
    private MenuService menuService;

    @Autowired
    private ItemService itemService;

    @Autowired
    private MealTypeService mealTypeService;

    @Autowired
    private MenuValidator menuValidator;

    @Autowired
    private DayEditor dayEditor;

    @Autowired
    private MealTypeEditor mealTypeEditor;

    @Autowired
    private ItemEditor itemEditor;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(menuValidator);

        binder.registerCustomEditor(MealType.class, mealTypeEditor);
        binder.registerCustomEditor(Day.class, dayEditor);
        binder.registerCustomEditor(Item.class, itemEditor);
        binder.registerCustomEditor(List.class, new CustomCollectionEditor(List.class, false));
    }

    @RequestMapping(value = "/list", method = {RequestMethod.GET, RequestMethod.POST})
    public String list(ModelMap modelMap) {
        modelMap.addAttribute("menu", new Menu());

        List<Map> menus = process(menuService.getMenus());
        modelMap.addAttribute("menus", menus);

        List<MealType> mealTypes = mealTypeService.getMealTypes();
        modelMap.addAttribute("mealTypes", mealTypes);

        List<Day> days = Arrays.asList(Day.values());
        modelMap.addAttribute("days", days);

        return SHOW;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String edit(@Validated @ModelAttribute("menu") Menu menu,
                       Errors errors,
                       ModelMap modelMap) {

        if (errors.hasErrors()) {
            setupReferenceData(menu, modelMap);
            return SHOW;
        }

        menu = menuService.getMenuByMealTypeAndDay(menu.getMealType(), menu.getDay());

        if (menu.isNew()) {
            menu = menuService.addMenu(menu);
        }

        modelMap.addAttribute("menu", menu);

        List<Item> items = itemService.getItems();
        modelMap.addAttribute("items", items);

        return EDIT;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@ModelAttribute("menu") Menu menu) {
        Menu newMenu = menuService.getMenuById(menu.getId());

        newMenu.setItemList(menu.getItemList());

        menuService.updateMenu(newMenu);

        return Utility.redirect(LIST_URL);
    }

    @RequestMapping(value = "/remove", method = RequestMethod.GET)
    public String remove(@RequestParam(value = "id") int menuId) {
        menuService.removeMenu(menuId);

        return Utility.redirect(LIST_URL);
    }

    private void setupReferenceData(Menu menu, ModelMap modelMap) {
        modelMap.put("menu", menu);
    }

    public List<Map> process(List<Menu> menus) {
        List<Map> list = new ArrayList<>();
        for (Menu menu : menus) {
            Map map = new HashMap<>();

            map.put("menuId", menu.getId());
            map.put("day", menu.getDay());
            map.put("mealType", menu.getMealType().getName());

            String items = "";
            boolean flag = false;
            for (Item item : menu.getItemList()) {
                if (flag) {
                    items += ", ";
                }
                items += item.getName();
                flag = true;
            }
            if (items.equals("")) {
                continue;
            }
            map.put("items", items);

            list.add(map);
        }

        return list;
    }
}
